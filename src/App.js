import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./components/Home";
import Calculate from "./components/Calculate";
import { Setting } from "./components/Setting";
import { Help } from "./components/Help";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/calculate" component={Calculate} />
        <Route exact path="/setting" component={Setting} />
        <Route exact path="/help" component={Help} />
      </Switch>
    </Router>
  );
}

export default App;
