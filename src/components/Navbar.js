import React from "react";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <>
      {/* Only for host */}
      <nav className="navbar navbar-light bg-light shadow-sm mb-2 bg-white rounded">
        <div className="btn-block btn-group">
          <Link className="btn btn-outline-info active" to="/">
            Home
          </Link>
          <Link className="btn btn-outline-info" to="/calculate">
            Pay Counter
          </Link>
          <Link className="btn btn-outline-info" to="/setting">
            Settings
          </Link>
          <Link className="btn btn-outline-info" to="/help">
            Help
          </Link>
        </div>
      </nav>
    </>
  );
}
