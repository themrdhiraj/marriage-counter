import React from "react";
import Navbar from "./Navbar";
import { Loading } from "./Loading";

export const Help = () => {
  return (
    <>
      <Navbar />
      <Loading />
      <h1 className="text-center">Help goes here</h1>
    </>
  );
};
