import React from "react";
import Navbar from "./Navbar";

export const Setting = () => {
  return (
    <React.Fragment>
      <Navbar />
      <div className="mx-auto col-md-5">
        <div className="card border-info">
          <div className="card-header border-info">Settings</div>
          <div className="card-body border-info">
            <form>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Add new player</div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  placeholder="New player name"
                />
              </div>
              <button className="btn btn-info btn-block">Add</button>
            </form>
            <hr />
            <select className="form-control">
              <option disabled selected>
                Add more amount?
              </option>
              <option>Add 100 for each</option>
              <option>Add 200 for each</option>
              <option>Add 500 for each</option>
            </select>
            <hr />
            <button className="btn btn-dark btn-block btn-lg">
              Reset game
            </button>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
