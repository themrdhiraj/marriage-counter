import React from "react";
import Navbar from "./Navbar";
import { Help } from "./Help";

function Calculate() {
  return (
    <>
      {/* For every player */}
      <Navbar />
      <div className="mx-auto col-md-5">
        <div className="card border-info">
          <div className="card-header border-info">
            Dhiraj Shrestha •{" "}
            <span className="badge badge-pill badge-info">Round 1</span>
          </div>
          <div className="card-body border-info">
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  <span className="badge badge-pill badge-info">
                    Pay : <b>Ram Khadka</b>
                  </span>
                </div>
              </div>
              <input type="number" className="form-control" placeholder="4" />
            </div>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  <span className="badge badge-pill badge-info">
                    Pay : <b>Ram Khadka</b>
                  </span>
                </div>
              </div>
              <input type="number" className="form-control" placeholder="4" />
            </div>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  <span className="badge badge-pill badge-info">
                    Pay : <b>Ram Khadka</b>
                  </span>
                </div>
              </div>
              <input type="number" className="form-control" placeholder="4" />
            </div>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">
                  <span className="badge badge-pill badge-info">
                    Pay : <b>Ram Khadka</b>
                  </span>
                </div>
              </div>
              <input type="number" className="form-control" placeholder="4" />
            </div>
            <hr />
            <button className="btn btn-warning btn-block border-danger">
              Pay
              <span className="badge badge-danger badge-pill">Total: 4</span>
            </button>
          </div>
          <div className="card-footer border-info">
            <span className="badge badge-info">
              Your Amount :{" "}
              <span className="badge badge-pill badge-warning">
                <b>500</b>
              </span>
            </span>
          </div>
        </div>
      </div>
      <hr />
      <div className="mx-auto col-md-5 mb-5">
        <div className="card border-info">
          <div className="card-header border-info">Game log • You paid</div>
          <div className="card-body border-info">
            <div className="table-responsive">
              <table
                className="table table-striped table-bordered"
                style={{ whiteSpace: "nowrap" }}
              >
                <thead>
                  <tr>
                    <th>Round</th>
                    <th>Player 1</th>
                    <th>Player 2</th>
                    <th>Player 3</th>
                    <th>Player 4</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>
                      <span className="badge badge-danger">20</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">21</span>
                    </td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>
                      <span className="badge badge-danger">20</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">21</span>
                    </td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>
                      <span className="badge badge-danger">20</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">21</span>
                    </td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>
                      <span className="badge badge-danger">20</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">0</span>
                    </td>
                    <td>
                      <span className="badge badge-danger">21</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Calculate;
