import React from "react";
import Navbar from "./Navbar";

export default function Home() {
  return (
    <React.Fragment>
      <Navbar />
      {/* This hides after adding pepole */}
      <div className="mx-auto col-md-5 mb-2">
        <div className="card border-info">
          <div className="card-header border-info">Home</div>
          <div className="card-body border-info">
            <form>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Total players</div>
                </div>
                <input type="number" className="form-control" placeholder="4" />
              </div>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Initial Amount</div>
                </div>
                <input
                  type="number"
                  className="form-control"
                  placeholder="500"
                />
              </div>
              {/* On submit generate key where they all connect */}
              <button className="btn btn-info btn-block btn-lg">
                Generate key
              </button>
            </form>
          </div>
        </div>
      </div>
      {/* This hides before adding people */}
      <div className="mx-auto col-md-5">
        <div className="card border-info">
          <div className="card-header border-info">Edit names</div>
          <div className="card-body border-info">
            <form>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Player Name</div>
                </div>
                <input
                  defaultValue="Player 1"
                  type="text"
                  className="form-control"
                  placeholder="Ram"
                />
              </div>
              <button className="btn btn-info btn-block">Update</button>
            </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
