import React from "react";

export const Loading = () => {
  return (
    <div className="text-center">
      <div
        className="spinner-grow text-info"
        style={{ width: "5rem", height: "5rem" }}
        role="status"
      >
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  );
};
